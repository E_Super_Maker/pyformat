directory = input("Input deletion directory. Do not include the directory PyFormat's in. ")
print("Selected " + directory + ".")
def selectType():
    inputType = input("Type D for directory, or F for file.")
    if inputType == "F":
        print("Selected type file.")
        os.remove(directory)
        print("Removed file.)
    elif inputType == "T":
        print("Selected type directory.")
        shutil.rmtree(directory)
        print("Removed directory")
    else:
        print("Invalid input. Restarting.")
        selectType()