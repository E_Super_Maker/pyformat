##Installation
Run the PyFormat file that came with the clone. You must have already installed Python 3.
##Usage
It will first ask you for a file/directory. Next, it will ask you if it's a file or directory.
##License
This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/.